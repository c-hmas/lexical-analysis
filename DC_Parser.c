/* Humberto Alvarez | 2020 
 * 
 * Parser for "DC" programming language. Program scans the input file given through command line argument and stores the pairs of a lexems and tokens within it in a Linked List.
 * These tokens are then checked to validate the syntax.
 * 
 * This program uses a modified version of the DC BNF, with left recursion removed:
 * 
 * P   ::= S
 * S   ::= V := E S' | read ( V ) S' | write ( V ) S' | while C do S od S' 
 * S’  ::= ; S S’ | empty
 * C   ::= E < E | E > E | E = E | E <> E| E >= E | E <= E
 * E   ::= T E'
 * E'  ::= + T E' | - T E' | empty
 * T   ::= F T' 
 * T’  ::= * F T' | / F T’ | empty
 * F   ::= ( E ) | N | V
 * V   ::= var
 * N   ::= num
 * */

#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<ctype.h>

void getTokens(char *line);
int isDelimiter(char c);
char* getCharToken(char lexeme);
char* getStringToken(char* lexeme);
char* getString(char* line, int start, int end);
int isKey(char* s);
int isInteger(char* s);
int isIntegerChar(char c);
int isValidIdentifier(char* s);
void unknownSymbols(char* s);


struct Node // Linked List structure used to store program's lexeme/token pairs in the order which the appear in a file
{
    char* lexeme;
    char* token;
    struct Node* next;
};

void insert_node(struct Node** head_ref, char* new_lexeme, char* new_token);

void statement();
void statement_prime();
void expression();
void expression_prime();
void comparison();
void term();
void term_prime();
void factor();

struct Node* head;  // Node that marks the beginning of the linked lsit
struct Node* current; // Node pointer used to traverse the Linked List

void statement() // Definition of S rules
{
    if(!strcmp(current->token, "IDENT"))
    {
        current = current->next;
        if(!strcmp(current->token, "ASSIGN_OP") )
        {
            current = current->next;
            expression();
            statement_prime();
        }
        else
        {
            printf("\nError encounter: The next lexeme was %s and the next token was %s\n\n", current->lexeme, current->token); 
            exit(1); 
        }

    }
    else if( (!strcmp(current->token, "KEY_READ")) || (!strcmp(current->token, "KEY_WRITE" )) )
    {
        current = current->next;
        if(!strcmp(current->token, "LEFT_PAREN" ))
        {
            current = current->next;
            if(!strcmp(current->token, "IDENT" ))
            {
                current = current->next;
                if(!strcmp(current->token, "RIGHT_PAREN" ))
                {
                    current = current->next;
                    statement_prime();
                }
                else
                {
                    printf("\nError encounter: The next lexeme was %s and the next token was %s\n\n", current->lexeme, current->token); 
                    exit(1); 
                }

            }
        }
    }
    else if(!strcmp(current->token, "KEY_WHILE" ))
    {
        current = current->next;
        comparison();
        if(!strcmp(current->token, "KEY_DO" ))
        {
            current = current->next;
            statement();
            if(!strcmp(current->token, "KEY_OD" ))
            {
                current = current->next;
                statement_prime();
            }
            
        }
    }
}

void statement_prime() // Definition of S' rules
{
    if( (!strcmp(current->token, "SEMICOLON" )))
    {
        current = current->next;
        if( (strcmp(current->token, "UNKNOWN")) && (strcmp(current->token, "$")) && (strcmp(current->token, "KEY_OD")) )
        {
            statement();
            statement_prime();
        }
        else
        {
            printf("\nError encounter: The next lexeme was %s and the next token was %s\n\n", current->lexeme, current->token); 
            exit(1); 
        } 
    } 
}

void comparison() // Definition of C rules
{
    expression();
    if( (!strcmp(current->token, "LESSER_OP")) ||
        (!strcmp(current->token, "GREATER_OP")) ||
        (!strcmp(current->token, "EQUAL_OP")) ||
        (!strcmp(current->token, "NEQUAL_OP")) ||
        (!strcmp(current->token, "LEQUAL_OP")) ||
        (!strcmp(current->token, "GEQUAL_OP")) )
    {
        current = current->next;
        expression();
    }
}

void expression() // Definition of E rules
{
    term();
    if( (!strcmp(current->token, "ADD_OP")) ||
        (!strcmp(current->token, "SUB_OP")) )
    {
        expression_prime();
    }
}

void expression_prime() // Definition of E' rules
{
    if( (!strcmp(current->token, "ADD_OP")) ||
        (!strcmp(current->token, "SUB_OP")))
    {
        current = current->next;
        term();
        if( (!strcmp(current->token, "ADD_OP")) ||
        (!strcmp(current->token, "SUB_OP")) )
        {
            expression_prime();
        }
    }
}

void term() // Definition of T rules
{
    factor();
    if( (!strcmp(current->token, "DIV_OP")) ||
        (!strcmp(current->token, "MULT_OP")) )
    {
        term_prime();
    }
    
}

void term_prime() // Definition of T' rules
{
    if( (!strcmp(current->token, "MULT_OP")) ||
        (!strcmp(current->token, "DIV_OP")) )
    {
        current = current->next;
        factor();
        if( (!strcmp(current->token, "DIV_OP")) ||
            (!strcmp(current->token, "MULT_OP")) )
        {
            term_prime();
        }
    }
}

void factor() // Definition of F rules
{
    if(!strcmp(current->token, "LEFT_PAREN"))
    {
        current = current->next;
        expression();
        if(!strcmp(current->token, "RIGHT_PAREN"))
        {
            current = current->next;
        }
    }
    else if( (!strcmp(current->token, "INT_LIT")) ||
             (!strcmp(current->token, "IDENT")))
    {
        current = current->next;
    }

}

void insert_node(struct Node** head_ref, char* new_lexeme, char* new_token) // function to insert new node into specified Linked List
{
    struct Node* new_node = (struct Node*) malloc(sizeof(struct Node));
    struct Node *last = *head_ref;
  
    new_node->lexeme = new_lexeme;
    new_node->token = new_token;
    new_node->next = NULL; 

    if (*head_ref == NULL) 
    { 
       *head_ref = new_node; 
       return; 
    }   

    while (last->next != NULL) 
        last = last->next;
    
    last->next = new_node; 
    return;     

}

int main(int argc, char* argv[])
{
    printf("\nDC Parser\n");

    FILE *fp;

    char line[128];

    if(!argv[1])
    {
        printf("\nError encounter: missing input file\n\n");
        exit(2);
    }
    
    fp = fopen(argv[1], "r");

    if(!fp)
    {
        printf("\nError encounter: File not found\n\n");
        exit(3);
    }
    else // checks that the file is not actually empty
    {
        fseek(fp, 0, SEEK_END);
        int notEmpty = ftell(fp);
        if(!notEmpty)
        {
            printf("\nError encounter: The next lexeme was NULL and the next token was NULL\n\n"); 
            exit(1);
        }
        rewind(fp);
    }

    while(fgets(line, 100, fp))
    {
        // First, the file is scanned for all its lexeme/token pairs, which are then stored in a linked list 
        getTokens(line);
    }

    fclose(fp);
    insert_node(&head, "$", "$"); // final Node with token '$' to mark the end of the program; signifies a valid parsing.
    current = head;
    
    statement();

    if(!strcmp(current->token, "$"))
        printf("\nSyntax Validated");   
    else
    {
        printf("\nError encounter: The next lexeme was %s and the next token was %s\n\n", current->lexeme, current->token); 
        exit(1); 
    }
    
    return(0);
}

void getTokens(char *line) //prints all lexeme/token pairs in one line
{
    int start = 0;
    int end = 0;
    int length = strlen(line); //length of the current line

    while(end <= length && start <= end)
    {            
        if(!isDelimiter(line[end]) )
            end++;
        else
        {
            char* string = getString(line, start, end - 1);
            if(isKey(string))
                insert_node(&head, string, getStringToken(string));
            else if(isInteger(string))
                insert_node(&head, string, "INT_LIT");
            else
            {
                if(strcmp(string, "") != 0)
                {
                    if(isValidIdentifier(string))
                        insert_node(&head, string, "IDENT");
                    else
                        unknownSymbols(string);
                }
            }       
            start = end;

            if (line[end] == ':' &&  line[end + 1] == '=')
            {
                insert_node(&head, ":=", "ASSIGN_OP");
                end += 2;
                start = end;
            }
            else if(line[end] == '<')
            {
                if(line[end + 1] == '>')
                {
                    insert_node(&head, "<>", "NEQUAL_OP");
                    end++;
                }
                else if(line[end + 1] == '=')
                {
                    insert_node(&head, "<=", "LEQUAL_OP");
                    end++;
                }
                else
                    insert_node(&head, "<", "LESSER_OP");

                end++;
                start = end;  
            }

            else if(line[end] == '>')
            {
                if(line[end + 1] == '=')
                {   
                    insert_node(&head, ">=", "GEQUAL_OP");
                    end++;
                }
                else
                    insert_node(&head, ">", "GREATER_OP");
                end++;
                start = end;
            }

            else if(isDelimiter(line[end]))
            {
                if(line[end] != ' ' && (line[end] != '\r') && (line[end] != '\n') && (line[end] != '\t'))
                {
                   char *temp = getString(line, end, end);

                   insert_node(&head, temp, getCharToken(line[end]));
                }
                end++;
                start = end;
            }
        }

        if(!isDelimiter(line[end]) && end == length) //case where final character(s) is not a delimiter
        {
            char* string = getString(line, start, end - 1);

            if(isKey(string))
                insert_node(&head, string, getStringToken(string));
            else if(isInteger(string))
                insert_node(&head, string, "INT_LIT");
            else
            {
                if(strcmp(string, "") != 0)
                {
                    if(isValidIdentifier(string))
                        insert_node(&head, string, "IDENT");
                    else
                        unknownSymbols(string);
                    
                }
            }
        }
    }
}

char* getString(char* line, int start, int end) //returns substring that begins at index 'start' and ends with index 'end' in the specified string
{
    int i;
    char* string = (char*)malloc(sizeof(char) * (end - start + 2));
    for(i = start; i <= end; i++)
    {
        if(line[i] != '\t')
            string[i - start] = line[i];
        else
            start++;
    }
    string[end - start + 1] = '\0';
    return(string);
}

int isDelimiter(char c) //returns true (1) if char is a delimiter
{
    if  (
         c == '\t' ||
         c == '\n' ||
         c == '\r' ||
         c == ';' ||
         c == '(' ||
         c == ')' ||
         c == ' ' ||
         c == '=' ||
         c == '+' ||
         c == '-' ||
         c == '*' ||
         c == '/' ||
         c == ':' ||
         c == '<' ||
         c == '>'   )
    {
        return 1;
    }
    return 0;
}

int isKey(char* s) //returns true (1) if string is a keyword
{
    if( (!strcmp(s, "read"))  ||
        (!strcmp(s, "write")) ||
        (!strcmp(s, "while")) ||
        (!strcmp(s, "do"))    ||
        (!strcmp(s, "od"))       )
    {
        return 1;
    }
    return 0;
}

int isInteger(char* s)  //returns true (1) is an int literal
{
    int length = strlen(s);
    int i;

    if(length == 0)
        return 0;
    for(i = 0; i < length;i++)
    {
        if( (s[i] != '0') && (s[i] != '1') && (s[i] != '2') &&
            (s[i] != '3') && (s[i] != '4') && (s[i] != '5') &&
            (s[i] != '6') && (s[i] != '7') && (s[i] != '8') &&
            (s[i] != '9')                                       )
        {
            return 0;
        }
    }
    return 1;
}

int isValidIdentifier(char* s) //returns true (1) if the given string is a valid identifier. i.e: the string contains only letters a to z
{
    int length = strlen(s);
    int i;
    for(i = 0; i < length;i++)
    {
        if (!(s[i] >= 'a' && s[i] <= 'z'))
            return 0;
    }
    return 1;
    
}

char* getCharToken(char lexeme) //returns token from specified lexeme of a single character
{
    switch(lexeme)
{
        case '(':
            return "LEFT_PAREN";
            break;
        case ')':
            return "RIGHT_PAREN";
            break;
        case ';':
            return "SEMICOLON";
            break;
        case '+':
            return "ADD_OP";
            break;
        case '-':
            return "SUB_OP";
            break;
        case '*':
            return "MULT_OP";
            break;
         case '/':
            return "DIV_OP";
            break;
        case '=':
            return "EQUAL_OP";
            break;
    }
    return "UNKNOWN";
}

char* getStringToken(char* lexeme) //returns token from specified lexeme
{
    if(!strcmp(lexeme, "read"))
        return "KEY_READ";
    else if(!strcmp(lexeme, "write"))
        return "KEY_WRITE";
    else if(!strcmp(lexeme, "while"))
        return "KEY_WHILE";
    else if(!strcmp(lexeme, "do"))
        return "KEY_DO";
    else if(!strcmp(lexeme, "od"))
        return "KEY_OD";
    return("UNKNOWN");
}


void unknownSymbols(char* s) //function that handles variables with unknown symbols
{
    int start = 0; int end = 0;
    int i;
    
    for(i = 0;i<strlen(s);i++)
    {
        start = i; end = i;
        if(s[i] >= 'a' && s[i] <= 'z')
        {
            while((s[i] >= 'a' && s[i] <= 'z') && i < strlen(s))
            {
                i++;
                end++;
            }
            end--;
            char* temp = getString(s, start, end);
            insert_node(&head, temp, "IDENT");
                        i--;
        }
        else if(isIntegerChar(s[i]) )
        {
            while((isIntegerChar(s[i])) && i < strlen(s))
            {
                i++;
                end++;
            }
            end--;
            char* temp = getString(s, start, end);
            insert_node(&head, temp, "INT_LIT");
            i--;
        }
        else 
        {

            char* temp = getString(s, i, i);
            insert_node(&head, temp, "UNKNOWN");

            //printf("%c\tUNKNOWN",s[i]);
        }
    }
    return;
}

int isIntegerChar(char c) // checks if specified char contains an integer
{
    if( (c != '0') && (c != '1') && (c != '2') &&
        (c != '3') && (c != '4') && (c != '5') &&
        (c != '6') && (c != '7') && (c != '8') &&
        (c != '9')                                )
        {
            return 0;
        }
        return 1;
}

/* Humberto Alvarez | 2020 
 * 
 * Lexical Analyzer for "DC" programming language. Program returns pair of lexemes and tokens of a given program from
 * the input file given as Command Line Argument
 * 
 * DC Grammar:
 * 
 * P ::= S
 * S ::= V:=E | read(V) | write(V) | while C do S od | S;S C ::= E < E | E > E | E = E | E <> E | E <= E | E >= E
 * E ::= T | E + T | E - T
 * T ::= F | T * F | T / F
 * F ::= (E) | N | V
 * V ::= a | b | ... | y | z | aV | bV | ... | yV | zV
 * N ::= 0 | 1 | ... | 8 | 9 | 0N | 1N | ... | 8N | 9N
 * 
 */


#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<ctype.h>

void getTokens(char *line);
int isDelimiter(char c);
char* getCharToken(char lexeme);
char* getStringToken(char* lexeme);
char* getString(char* line, int start, int end);
int isKey(char* s);
int isInteger(char* s);
int isIntegerChar(char c);
int isValidIdentifier(char* s);
void unknownSymbols(char* s);

int main(int argc, char* argv[])
{
    FILE *fp;
    char line[128];

    fp = fopen(argv[1], "r");

    if(!fp)
    {
        exit(EXIT_FAILURE);
    }

    printf("DC Analyzer\n");

    while(fgets(line, 100, fp))
    {
        getTokens(line);
    }
    printf("\n");
    fclose(fp);
    return(0);
}
void getTokens(char *line) //prints all lexeme/token pairs in one line
{
    int start = 0;
    int end = 0;
    int length = strlen(line); //length of the current line

    while(end <= length && start <= end)
    {            
        if(!isDelimiter(line[end]) )
            end++;
        else
        {
            char* string = getString(line, start, end - 1);
            if(isKey(string))
                printf("\n%s\t%s", string, getStringToken(string));
            else if(isInteger(string))
                printf("\n%s\tINT_LIT", string);
            else
            {
                if(strcmp(string, "") != 0)
                {
                    if(isValidIdentifier(string))
                        printf("\n%s\tIDENT", string);
                    else
                        unknownSymbols(string);
                }
            }       
            start = end;

            if (line[end] == ':' &&  line[end + 1] == '=')
            {
                printf("\n:=\tASSIGN_OP");
                end += 2;
                start = end;
            }
            else if(line[end] == '<')
            {
                if(line[end + 1] == '>')
                {
                    printf("\n<>\tNEQUAL_OP");
                    end++;
                }
                else if(line[end + 1] == '=')
                {
                    printf("\n<=\tLEQUAL_OP");
                    end++;
                }
                else
                    printf("\n<\tLESSER_OP");

                end++;
                start = end;  
            }

            else if(line[end] == '>')
            {
                if(line[end + 1] == '=')
                {   
                    printf("\n>=\tGEQUAL_OP");
                    end++;
                }
                else
                    printf("\n>\tGREATER_OP");
                end++;
                start = end;
            }

            else if(isDelimiter(line[end]) )
            {
                if(line[end] != ' ' && (line[end] != '\r') && (line[end] != '\n') && (line[end] != '\t'))
                    printf("\n%c\t%s", line[end], getCharToken(line[end]));
                end++;
                start = end;
            }
        }
        if(!isDelimiter(line[end]) && end == length) //case where final character(s) is not a delimiter
        {
            char* string = getString(line, start, end - 1);

            if(isKey(string))
                printf("\n%s\t%s", string, getStringToken(string));
            else if(isInteger(string))
                printf("\n%s\tINT_LIT", string);
            else
            {
                if(strcmp(string, "") != 0)
                {
                    if(isValidIdentifier(string))
                        printf("\n%s\tIDENT", string);
                    else
                        unknownSymbols(string);
                    
                }
            }
        }
    }
}

char* getString(char* line, int start, int end) //returns substring that begins at index 'start' and ends with index 'end' in the specified string
{
    int i;
    char* string = (char*)malloc(sizeof(char) * (end - start + 2));
    for(i = start; i <= end; i++)
    {
        if(line[i] != '\t')
            string[i - start] = line[i];
        else
            start++;
    }
    string[end - start + 1] = '\0';
    return(string);
}

int isDelimiter(char c) //returns true (1) if char is a delimiter
{
    if  (
         c == '\t' ||
         c == '\n' ||
         c == '\r' ||
         c == ';' ||
         c == '(' ||
         c == ')' ||
         c == ' ' ||
         c == '=' ||
         c == '+' ||
         c == '-' ||
         c == '*' ||
         c == '/' ||
         c == ':' ||
         c == '<' ||
         c == '>'   )
    {
        return 1;
    }
    return 0;
}

int isKey(char* s) //returns true (1) if string is a keyword
{
    if( (!strcmp(s, "read"))  ||
        (!strcmp(s, "write")) ||
        (!strcmp(s, "while")) ||
        (!strcmp(s, "do"))    ||
        (!strcmp(s, "od"))       )
    {
        return 1;
    }
    return 0;
}

int isInteger(char* s)  //returns true (1) is an int literal
{
    int length = strlen(s);
    int i;

    if(length == 0)
        return 0;
    for(i = 0; i < length;i++)
    {
        if( (s[i] != '0') && (s[i] != '1') && (s[i] != '2') &&
            (s[i] != '3') && (s[i] != '4') && (s[i] != '5') &&
            (s[i] != '6') && (s[i] != '7') && (s[i] != '8') &&
            (s[i] != '9')                                       )
        {
            return 0;
        }
    }
    return 1;
}

int isValidIdentifier(char* s) //returns true (1) if the given string is a valid identifier. i.e: the string contains only letters a to z
{
    int length = strlen(s);
    int i;
    for(i = 0; i < length;i++)
    {
        if (!(s[i] >= 'a' && s[i] <= 'z'))
            return 0;
    }
    return 1;
    
}

char* getCharToken(char lexeme) //returns token from specified lexeme of a single character
{
    switch(lexeme)
{
        case '(':
            return "LEFT_PAREN";
            break;
        case ')':
            return "RIGHT_PAREN";
            break;
        case ';':
            return "SEMICOLON";
            break;
        case '+':
            return "ADD_OP";
            break;
        case '-':
            return "SUB_OP";
            break;
        case '*':
            return "MULT_OP";
            break;
         case '/':
            return "DIV_OP";
            break;
        case '=':
            return "EQUAL_OP";
            break;
    }
    return "UNKNOWN";
}

char* getStringToken(char* lexeme) //returns token from specified lexeme
{
    if(!strcmp(lexeme, "read"))
        return "KEY_READ";
    else if(!strcmp(lexeme, "write"))
        return "KEY_WRITE";
    else if(!strcmp(lexeme, "while"))
        return "KEY_WHILE";
    else if(!strcmp(lexeme, "do"))
        return "KEY_DO";
    else if(!strcmp(lexeme, "od"))
        return "KEY_OD";
    return("UNKNOWN");
}


void unknownSymbols(char* s) //function that handles variables with unknown symbols
{
    int i;
    for(i = 0;i<strlen(s);i++)
    {
        printf("\n");
        if(s[i] >= 'a' && s[i] <= 'z')
        {
            while((s[i] >= 'a' && s[i] <= 'z') && i < strlen(s))
            {
                printf("%c", s[i]);
                i++;
            }
            printf("\tIDENT");
            i--;
        }
        else if( isIntegerChar(s[i]) )
        {
            while((isIntegerChar(s[i])) && i < strlen(s))
            {
                printf("%c", s[i]);
                i++;
            }
            printf("\tINT_LIT");
            i--;
        }
        else 
            printf("%c\tUNKNOWN",s[i]);
    }
    return;
}

int isIntegerChar(char c) // checks if specified char contains an integer
{
    if( (c != '0') && (c != '1') && (c != '2') &&
        (c != '3') && (c != '4') && (c != '5') &&
        (c != '6') && (c != '7') && (c != '8') &&
        (c != '9')                                )
        {
            return 0;
        }
        return 1;
}